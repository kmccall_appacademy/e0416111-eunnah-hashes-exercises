# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words_hash = {}
  str.split.each do |word|
    words_hash[word] = word.length
  end
  words_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |key, value| value }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
counter_hash = Hash.new(0)
  word.each_char do |letter|
    counter_hash[letter] = counter_hash[letter] + 1
  end
  counter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter_hash = Hash.new(0)
    arr.each do |el|
      counter_hash[el] = counter_hash[el] + 1
    end
    counter_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counter_hash = { even: 0, odd: 0 }
  numbers.each do |num|
    if num.even?
      counter_hash[:even] = counter_hash[:even] + 1
    else
      counter_hash[:odd] = counter_hash[:odd] + 1
    end
  end
  counter_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ["a", "e", "i", "o", "u"]
  counter_hash = Hash.new(0)
    string.each_char do |letter|
      if vowels.include?(letter)
        counter_hash[letter] = counter_hash[letter] + 1
      end
    end
  counter_hash.sort_by { |key, value| value }.last[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  fall_and_winter_students = []
  students.each do |key, value|
    fall_and_winter_students << key if value >= 7
  end

  results = []
  fall_and_winter_students.each_index do |index1|
    ((index1 + 1)...fall_and_winter_students.length).each do |index2|
      results << [fall_and_winter_students[index1], fall_and_winter_students[index2]]
    end
  end
  results
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_counter = Hash.new(0)
  specimens.each do |key, value|
    species_counter[key] = species_counter[key] + 1
  end

  number_of_species = species_counter.length

  smallest_population_size = species_counter.values.sort.first
  largest_population_size = species_counter.values.sort.last

  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counter = character_count(normal_sign)
  vandalized_counter = character_count(vandalized_sign)
  vandalized_counter.each do |key, value|
    if normal_counter[key] < value
      return false
    end
  end
  true
end

def character_count(str)
  punctuation = ";!',:/. "
  str.downcase!
  str.delete!(punctuation)
  counter = Hash.new(0)
  str.each_char do |letter|
    counter[letter] = counter[letter] + 1
  end
  counter
end
